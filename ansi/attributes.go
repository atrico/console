package ansi

import (
	"fmt"
	"strings"

	"gitlab.com/atrico/console/ansi/color"
)

type Attributes interface {
	Foreground() color.Color
	Background() color.Color
	// Apply to a string (absolute as if no current attributes)
	ApplyTo(str string) string
	// Apply to a string and reset to no attributes
	ApplyWithResetTo(str string) string
	// Get a change to create this state from no attributes
	SetThis() AttributeChange
	// Get a change to reset this state
	ResetThis() AttributeChange

	// Get a change to change from this set to the new one
	CreateDeltaTo(newAttributes Attributes) AttributeChange
	// Get a change to change to this set from the old one
	CreateDeltaFrom(oldAttributes Attributes) AttributeChange
	// Modify these attributes with the change
	Modify(delta AttributeChange) Attributes
}

var NoAttributes Attributes = attributesImpl{color.None, color.None}

func NewAttributes(foreground, background color.Color) Attributes {
	return attributesImpl{foreground, background}
}
func NewAttributesFore(foreground color.Color) Attributes {
	return NewAttributes(foreground, color.None)
}
func NewAttributesBack(background color.Color) Attributes {
	return NewAttributes(color.None, background)
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------

type attributesImpl struct {
	foreground color.Color
	background color.Color
}

func (a attributesImpl) Foreground() color.Color {
	return a.foreground
}

func (a attributesImpl) Background() color.Color {
	return a.background
}

func (a attributesImpl) String() string {
	return fmt.Sprintf("[%s,%s]", a.Foreground(), a.Background())
}

// Apply to a string (absolute as if no current attributes)
func (a attributesImpl) ApplyTo(str string) string {
	return a.applyTo(str, false)
}

// Apply to a string and reset to no attributes
func (a attributesImpl) ApplyWithResetTo(str string) string {
	return a.applyTo(str, true)
}

// Get a change to create this state from no attributes
func (a attributesImpl) SetThis() AttributeChange {
	return a.CreateDeltaFrom(NoAttributes)
}

// Get a change to reset this state
func (a attributesImpl) ResetThis() AttributeChange {
	return a.CreateDeltaTo(NoAttributes)
}

// Get a change to change from this set to the new one
func (a attributesImpl) CreateDeltaTo(newAttributes Attributes) AttributeChange {
	return newAttributeChange(getDeltaCodes(a, newAttributes))
}

// Get a change to change to this set from the old one
func (a attributesImpl) CreateDeltaFrom(oldAttributes Attributes) AttributeChange {
	return newAttributeChange(getDeltaCodes(oldAttributes, a))
}

// Modify these attributes with the change
func (a attributesImpl) Modify(delta AttributeChange) Attributes {
	return modifyAttributes(a, delta)
}

func (a attributesImpl) applyTo(str string, reset bool) string {
	result := strings.Builder{}
	result.WriteString(a.SetThis().ApplyTo(str))
	if reset {
		result.WriteString(a.ResetThis().GetCodeString())
	}
	return result.String()
}
