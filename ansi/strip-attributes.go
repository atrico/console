package ansi

import (
	"strings"
)

// Strip the attributes from a string
func StripAttributes(str string) string {
	newStr := strings.Builder{}
	for _, attr := range ParseString(str) {
		newStr.WriteString(attr.String)
	}
	return newStr.String()
}

// Length ignoring attributes
func StrLen(str string) int {
	return len([]rune(StripAttributes(str)))
}
