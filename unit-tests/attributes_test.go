package unit_tests

import (
	"fmt"
	"testing"

	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"

	"gitlab.com/atrico/console/ansi"
	"gitlab.com/atrico/console/ansi/color"
)

func Test_Attributes_NoAttributes(t *testing.T) {
	// Arrange

	// Act
	attribs := ansi.NoAttributes
	fmt.Println(attribs)

	// Assert
	assert.That(t, attribs.Foreground(), is.EqualTo(color.None), "Foreground")
	assert.That(t, attribs.Background(), is.EqualTo(color.None), "Background")
}

func Test_Attributes_Set(t *testing.T) {
	// Arrange
	original := ansi.NoAttributes
	fore := randomColour()
	back := randomColour()
	target := ansi.NewAttributes(fore, back)
	fmt.Printf("%s => %s\n", original, target)

	// Act
	delta := target.SetThis()
	newAttributes := original.Modify(delta)
	fmt.Printf("= %s\n", newAttributes)

	// Assert
	assert.That(t, newAttributes.Foreground(), is.EqualTo(fore), "Foreground - no change")
	assert.That(t, newAttributes.Background(), is.EqualTo(back), "Background - no change")
}

func Test_Attributes_Modify(t *testing.T) {
	// Arrange
	fore1 := randomColour()
	back1 := randomColour()
	original := ansi.NewAttributes(fore1, back1)
	fore2 := fore1
	for fore2 == fore1 {
		fore2 = randomColour()
	}
	back2 := back1
	for back2 == back1 {
		back2 = randomColour()
	}
	target := ansi.NewAttributes(fore2, back2)
	fmt.Printf("%s => %s\n", original, target)

	// Act
	delta := original.CreateDeltaTo(target)
	newAttributes := original.Modify(delta)
	fmt.Printf("= %s\n", newAttributes)

	// Assert
	assert.That(t, newAttributes.Foreground(), is.EqualTo(fore2), "Foreground")
	assert.That(t, newAttributes.Background(), is.EqualTo(back2), "Background")
}

func Test_Attributes_Reset(t *testing.T) {
	// Arrange
	fore := randomColour()
	back := randomColour()
	original := ansi.NewAttributes(fore, back)
	fmt.Printf("%s => %s\n", original, ansi.NoAttributes)

	// Act
	delta := original.ResetThis()
	newAttributes := original.Modify(delta)
	fmt.Printf("= %s\n", newAttributes)

	// Assert
	assert.That(t, newAttributes.Foreground(), is.EqualTo(color.None), "Foreground")
	assert.That(t, newAttributes.Background(), is.EqualTo(color.None), "Background")
}

func randomColour() color.Color {
	val := anyValue.IntBetween(30, 38)
	if anyValue.Bool() {
		// Bright
		val = val + 60
	}
	return color.Color(val)
}
