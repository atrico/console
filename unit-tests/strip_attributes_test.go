package unit_tests

import (
	"fmt"
	"strings"
	"testing"

	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"

	"gitlab.com/atrico/console/ansi"
	"gitlab.com/atrico/console/ansi/color"
)

func Test_StripAttributes_NoAttributes(t *testing.T) {
	// Arrange
	str := anyValue.String()

	// Act
	stripped := ansi.StripAttributes(str)

	// Assert
	assert.That(t, stripped, is.EqualTo(str), "Identical")
}

func Test_StripAttributes_ForegroundColor(t *testing.T) {
	// Arrange
	fore := randomColour()
	attribs := ansi.NewAttributes(fore, color.None).SetThis()
	raw := anyValue.String()
	str := attribs.ApplyTo(raw)
	fmt.Println(str, ansi.ResetAll.GetCodeString())

	// Act
	stripped := ansi.StripAttributes(str)
	fmt.Println(stripped)

	// Assert
	assert.That(t, stripped, is.EqualTo(raw), "Attributes stripped")
}

func Test_StripAttributes_BackgroundColor(t *testing.T) {
	// Arrange
	back := randomColour()
	attribs := ansi.NewAttributes(color.None, back).SetThis()
	raw := anyValue.String()
	str := attribs.ApplyTo(raw)
	fmt.Println(str, ansi.ResetAll.GetCodeString())

	// Act
	stripped := ansi.StripAttributes(str)
	fmt.Println(stripped)

	// Assert
	assert.That(t, stripped, is.EqualTo(raw), "Attributes stripped")
}

func Test_StripAttributes_BothColors(t *testing.T) {
	// Arrange
	fore := randomColour()
	back := randomColour()
	attribs := ansi.NewAttributes(fore, back).SetThis()
	raw := anyValue.String()
	str := attribs.ApplyTo(raw)
	fmt.Println(str, ansi.ResetAll.GetCodeString())

	// Act
	stripped := ansi.StripAttributes(str)
	fmt.Println(stripped)

	// Assert
	assert.That(t, stripped, is.EqualTo(raw), "Attributes stripped")
}

func Test_StripAttributes_MultipleColorsDeltaTo(t *testing.T) {
	// Arrange
	fore1 := color.Red
	fore2 := color.Green
	back2 := color.Blue
	back3 := color.Yellow
	attribs1 := ansi.NewAttributes(fore1, color.None)
	attribs2 := ansi.NewAttributes(fore2, back2)
	attribs3 := ansi.NewAttributes(color.None, back3)
	delta1 := attribs1.SetThis()
	delta2 := attribs1.CreateDeltaTo(attribs2)
	delta3 := attribs2.CreateDeltaTo(attribs3)
	raw1 := anyValue.String()
	raw2 := anyValue.String()
	raw3 := anyValue.String()
	strB := strings.Builder{}
	strB.WriteString(delta1.ApplyTo(raw1))
	strB.WriteString(delta2.ApplyTo(raw2))
	strB.WriteString(delta3.ApplyTo(raw3))
	str := strB.String()
	fmt.Println(str, ansi.ResetAll.GetCodeString())

	// Act
	stripped := ansi.StripAttributes(str)
	fmt.Println(stripped)

	// Assert
	assert.That(t, stripped, is.EqualTo(raw1+raw2+raw3), "Attributes stripped")
}

func Test_StripAttributes_MultipleColorsDeltaFrom(t *testing.T) {
	// Arrange
	fore1 := color.Red
	fore2 := color.Green
	back2 := color.Blue
	back3 := color.Yellow
	attribs1 := ansi.NewAttributes(fore1, color.None)
	attribs2 := ansi.NewAttributes(fore2, back2)
	attribs3 := ansi.NewAttributes(color.None, back3)
	delta1 := attribs1.SetThis()
	delta2 := attribs2.CreateDeltaFrom(attribs1)
	delta3 := attribs3.CreateDeltaFrom(attribs2)
	raw1 := anyValue.String()
	raw2 := anyValue.String()
	raw3 := anyValue.String()
	strB := strings.Builder{}
	strB.WriteString(delta1.ApplyTo(raw1))
	strB.WriteString(delta2.ApplyTo(raw2))
	strB.WriteString(delta3.ApplyTo(raw3))
	str := strB.String()
	fmt.Println(str, ansi.ResetAll.GetCodeString())

	// Act
	stripped := ansi.StripAttributes(str)
	fmt.Println(stripped)

	// Assert
	assert.That(t, stripped, is.EqualTo(raw1+raw2+raw3), "Attributes stripped")
}
